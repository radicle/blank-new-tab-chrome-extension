// Restore options
document.addEventListener('DOMContentLoaded', function(){
  var textInput = document.getElementById("backgroundColor");
  var backgroundColor = localStorage["backgroundColor"];
  textInput.value = (typeof backgroundColor != 'undefined') ? backgroundColor : "";
});

// Save options
document.querySelector('#save').addEventListener('click', function(){
  var textInput = document.getElementById("backgroundColor");
  localStorage["backgroundColor"] = textInput.value;
});